#include <stdio.h>
int main()
{
    int i,n,small,smallpos,large,largepos,temp,a[20];
    printf("Enter the number of elements in the array:\n ");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    small=a[0];
    smallpos=0;
    large=a[0];
    largepos=0;
    for(i=0;i<n;i++)
    {
        if(a[i]<small)
        {
            small=a[i];
            smallpos=i;
        }
        if(a[i]>large)
        {
            large=a[i];
            largepos=i;
        }
    }
    temp= a[smallpos];
    a[smallpos]= a[largepos];
    a[largepos]= temp;
    printf("The array after interchanging smallest and largest element is\n");
    for(i=0;i<n;i++)
    {
        printf("%d\n",a[i]);
    }
    return 0;
}
