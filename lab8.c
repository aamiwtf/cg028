Q1)

#include<stdio.h>
#include<math.h>
int main()
{
int sum=0,n;
printf("enter an even number \n");
scanf("%d",&n);
for(int i=2;i<=n;i++)
{
if(i%2==0)
sum=sum+pow(i,2);
else
continue;
}
printf("sum of squares of first n even numbers is %d",sum);
return 0;
}


Q2)

#include<stdio.h>
#include<math.h>
int main()
{
float term,sum=0.0;
int n;
printf("enter a number\n");
scanf("%d",&n);
for(int i=1;i<=n;i++)
{
term=1/(pow(i,2));
sum=sum+term;
}
printf("sum of the series 1/1^2+1/2^2+1/3^2+1/4^2+.......1/n^2 is %f",sum);
return 0;
}
