#include<stdio.h>
void swap(int *a,int *b)
{
  int temp;
  temp=*a;
  *a=*b;
  *b=temp;
}
int main ()
{
int num1,num2;
printf("enter the values of two numbers\n");
scanf("%d%d",&num1,&num2);
printf("values b4 swapping\n");
printf ("no1:%d\nno2:%d\n",num1,num2);
swap(&num1,&num2);
printf("values after swapping\n");
printf ("no1:%d\nno2:%d",num1,num2);
return 0;
}